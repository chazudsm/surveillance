package com.survelliance.security.kigombola;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RegistrationActivity extends AppCompatActivity {
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        final EditText username =(EditText)findViewById(R.id.username);
        final EditText password=(EditText)findViewById(R.id.password);
        final TextView register1=(TextView)findViewById(R.id.register1);

        register1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent hama= new Intent(RegistrationActivity.this,LoginActivity.class);
                startActivity(hama);
            }
        });
    }
}
