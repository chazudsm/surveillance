package com.survelliance.security.kigombola;


import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.Request;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Charles John Temu on 7/25/2018.
 */

public class LoginRequest extends StringRequest {


    private static final String Login_REQUEST_URL="http://charlsapp.bongomixtv.com/login.php";
    private Map<String, String> params;

//    public LoginRequest( String email, String password, Response.Listener<String> listerner){
//        super(Method.POST,Login_REQUEST_URL,listerner,null);
//        params =new HashMap<>();
//        params.put("username",email);
//        params.put("password",password);
//    }

    public LoginRequest(String username, String password, Response.Listener<String> listener) {
        super(Method.POST,Login_REQUEST_URL,listener, null);
        params=new HashMap<>();
        params.put("username",username);
        params.put("password",password);


    }


//
//    public LoginRequest(String username, String password, Response.Listener<String> responseListener) {
//        super();
//    }


    @Override
    public Map<String, String> getParams() {
        return params;
    }

}
