package com.survelliance.security.kigombola;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        final EditText Username=(EditText) findViewById(R.id.username);
        final EditText epasssword=(EditText) findViewById(R.id.password);
        final TextView login=(TextView) findViewById(R.id.login);
        final TextView register=(TextView) findViewById(R.id.register);
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sepa=new Intent(LoginActivity.this,RegistrationActivity.class);
                startActivity(sepa);
            }
        });

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final  String username=Username.getText().toString();
                final String  password=epasssword.getText().toString();

                    Response.Listener<String> responseListener= new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonResponse = new JSONObject(response);
                                boolean success= jsonResponse.getBoolean("success");

                                if(success){

                                    Intent intent= new Intent(LoginActivity.this,MainActivity.class);
                                     startActivity(intent);

                                }else{
                                    AlertDialog.Builder builder= new AlertDialog.Builder(LoginActivity.this);
                                    builder.setMessage("Login failed")
                                            .setNegativeButton("Retry",null)
                                            .create()
                                            .show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    };

                    LoginRequest loginRequest= new LoginRequest(username,password,responseListener);
                    RequestQueue queue=Volley.newRequestQueue(LoginActivity.this);
                    queue.add(loginRequest);
                }
        });

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //ugashosoosososososoo
    }

}
